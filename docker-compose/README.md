该目录下为docker compose使用的几个例子。

| 路径                               | 说明                                   |
|----------------------------------|--------------------------------------|
| [mysql-8-single](mysql-8-single) | 独立启动一个mysql，端口映射到宿主机3306，数据mount到宿主机 |
| [nginx](nginx)                   | 启动一个nginx                            |
| [sharding-db](sharding-db)       | 一键部署一主一从mysql数据库。                    |
| [xxl-job-admin](xxl-job-admin)   | 一键部署分布式任务调度中心：xxl-job-admin。         |
