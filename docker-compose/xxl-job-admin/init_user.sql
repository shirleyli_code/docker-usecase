CREATE USER 'rw_user'@'%' IDENTIFIED WITH mysql_native_password BY 'rw_pwd';
GRANT all privileges ON *.* TO 'rw_user'@'%';
FLUSH PRIVILEGES;
